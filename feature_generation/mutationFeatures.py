#generate mutation features
import sys,os

#################
#### FUNCTIONS


def score_differences(mapping, uniprot, start, end):
	count = 0
	if mapping.has_key(uniprot):
		if start <= end:
			for i in range(start,end+1):
				if not mapping[uniprot].has_key(i):
					pass
				elif mapping[uniprot][i] == "*":
					count = count + 1
			return (1.0*count)/(end - start + 1)

	return count


#################
#####PATH VARIABLE

try:
	PulsePATH = os.environ['PULSE_PATH']
except KeyError:
	print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
	print 'TRYNG WITH WORKING DIR'
	PulsePATH = os.getcwd()

#################
##### INPUT FILES
try:
	readFrom = open(PulsePATH+'/param_files/mutations.inp', 'r')

#Read the file with the maping between a splicing and its anchor
	map_file = sys.argv[1]
	readFrom1 = open(map_file, 'r')

except IndexError:
	print 'Map output files must be provided'
	sys.exit()

#################
#### OUTPUT FILES

writeTo = open('mutation_read.out', 'w')

#################
#### Init VARs

uniprot_to_index_to_disease = {}


###############
#### MAIN


for line in readFrom:

	try:
		tokens = line.split()
		uniprot = tokens[0]
		index = int(tokens[1])
		type = tokens[3]

		if uniprot_to_index_to_disease.has_key(uniprot):
			uniprot_to_index_to_disease[uniprot][index] = "*"
		else:
			uniprot_to_index_to_disease[uniprot] = {index:"*"}
	except ValueError:
		print "Cannot parse: " + line.strip()



for line in readFrom1:
	tokens = line.split()
	#PARSING ID
	#Customize the line below if is need it
	#it depends of how it is the label of th events
	#This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33
	asid = tokens[0].split("_")[0]
	prot = tokens[1]
	#PARSING ID
	sstart = int(tokens[2])
	start = int(tokens[3])
	end = int(tokens[4])
	eend = int(tokens[5])


	c1Count = 0
	aCount = 0
	c2Count = 0
	canonicalAbsolute = 0


	if not uniprot_to_index_to_disease.has_key(prot):
		c1Count = 0
		aCount = 0
		c2Count = 0
		canonicalAbsolute = 0
		otherAbsolute = 0
	else:
		c1Count = score_differences(uniprot_to_index_to_disease, prot, sstart, start)
		aCount = score_differences(uniprot_to_index_to_disease, prot, start, end)
		c2Count = score_differences(uniprot_to_index_to_disease, prot, end, eend)


		protLen = int(line.split("\t")[7].strip())
		canonicalAbsolute = score_differences(uniprot_to_index_to_disease, prot, 1, protLen)



	print >> writeTo, tokens[0] + "\t" + prot + "\t" + repr(c1Count) + "\t" + repr(aCount) + "\t" + repr(c2Count) + "\t" + repr(canonicalAbsolute)


