#Combines all our feature tables into one huge feature table ready to be read by R
#Also generates some simple features not found in other feature tables

import sys,os

#################
#####PATH VARIABLE

def readFeatures(map, stream, duality):
    for line in stream:
        tokens = line.split()
        if duality:
            key = tokens[0] + "//" + tokens[1]
            rest = tokens[2:len(tokens)]
            map[key] = rest
        else:
            key = tokens[0]
            rest = tokens[1:len(tokens)]
            map[key] = rest


def obtainFeature(mapp, key):
    try:
        length = len(mapp[mapp.keys()[0]])
    except TypeError:
        length = 1

    list = ["0"]*length
    if mapp.has_key(key):
        list = mapp[key]

    return list

def convList(list):

    try:
        stringR = ""
        for i in list:
            stringR += i + ", "
        return stringR[0:len(stringR)-2]
    except TypeError:
        return repr(list)


#################
#####PATH VARIABLE

try:
    PulsePATH = os.environ['PULSE_PATH']
except KeyError:
    print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
    print 'TRYNG WITH WORKING DIR'
    PulsePATH = os.getcwd()

#################
##### INPUT FILES
try:
#Read the file with the maping between a splicing and its anchor
    map_file = sys.argv[1]
    readFrom = open(map_file, 'r')
#====================================================================================================================
#read  features and put them into map files

    readCore = open('core_read.out', 'r')
    readDegree = open('degree_read.out', 'r')
    readDisorder = open('disorder_read.out', 'r')
    readDomain = open('domain_read.out', 'r')
    readElm = open('elm_read.out', 'r')
    readEventCon = open('eventCon_read.out', 'r')
    readMutation = open('mutation_read.out', 'r')
    readPTM = open('ptm_read.out', 'r')
    readSequenceCon = open('sequenceCon_read.out', 'r')
    readTransmem = open('transmem_read.out', 'r')
#This feature must be make ad hoc for our data (Event classification)
#Please, read the supplementary Material
#or just ignore this feature, notice the global score distribution will be affected
    readTS = open(PulsePATH+'/param_files/AS_Event_Classification.txt', 'r')
except IndexError, e:
    print 'Map output files must be provided'
    sys.exit()
except IOError, e:
    print 'Features files %s not found' % str(e)
    sys.exit()



#################
#### OUTPUT FILES

#New Folder to save output
#if alredy exist, raise a warning and continue, althought raise the error

try:
    os.makedirs("ML_input")
except OSError, e:
    if e.errno != 17:
        raise
    print e
    pass

writeToAll = open('./ML_input/featuresHeaders.txt', 'w')
writeNamesAll = open('./ML_input/names.txt', 'w')


#################
#### Init VARs

#dual key features
core_read = {}
disorder_read = {}
domain_read = {}
elm_read = {}
mutation_read = {}
ptm_read = {}
transmem_read = {}

#single key features
degree_read = {}
sequenceCon_read = {}
eventCon_read = {}
ts_read = {}

#################
#### MAIN

readFeatures(core_read, readCore, True)
readFeatures(disorder_read, readDisorder, True)
readFeatures(domain_read, readDomain, True)
readFeatures(elm_read, readElm, True)
readFeatures(mutation_read, readMutation, True)
readFeatures(ptm_read, readPTM, True)
readFeatures(transmem_read, readTransmem, True)
readFeatures(degree_read, readDegree, False)
readFeatures(sequenceCon_read, readSequenceCon, False)
readFeatures(eventCon_read, readEventCon, False)

for line in readTS:
    tokens = line.split()
    asid = tokens[0]
    ts = 0
    if tokens[2] == "TS":
        ts = 1
    ts_read[asid] = ts

#====================================================================================================================


output_positiveI = []
output_uniprotI = []
output_restI = []
output_positiveE = []
output_uniprotE = []
output_restE = []

for line in readFrom:
    tokens = line.split()
    # PARSING ID
    #Customize the line below if is need it
    #it depends of how it is the label of th events
    #This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33
    asid = tokens[0]
    # PARSING ID
    prot = tokens[1]
    sstart = int(tokens[2])
    start = int(tokens[3])
    end = int(tokens[4])
    eend = int(tokens[5])
    canonicalLength = int(line.split("\t")[7].strip())
    otherLength = int(line.split("\t")[8].strip())

    tempLine = line.split("\t")[6].strip().replace("[","").replace("]","").replace("'","").replace(" ","")

    listOther = []
    if tempLine != "":
        listOther = tempLine.split(",")

    #check the list to see if it's uniprot (not empty), or positive and add the line to print which we get eventually.

    #at the end print these in order.
    # PARSING ID
    #Customize the line below if is need it
    dualKey = asid + "//" + prot

    singleKey = asid
    # PARSING ID


    #obtain feature line from various feature files.
    coreFeatures = obtainFeature(core_read, dualKey)
    disorderFeatures = obtainFeature(disorder_read, dualKey)
    domainFeatures = obtainFeature(domain_read, dualKey)
    elmFeatures = obtainFeature(elm_read, dualKey)
    mutationFeatures = obtainFeature(mutation_read, dualKey)
    ptmFeatures = obtainFeature(ptm_read, dualKey)
    transmemFeatures = obtainFeature(transmem_read, dualKey)

    degreeFeatures = obtainFeature(degree_read, singleKey)
    sequenceConFeatures = obtainFeature(sequenceCon_read, singleKey)
    eventConFeatures = obtainFeature(eventCon_read, singleKey)
    tsFeatures = obtainFeature(ts_read, singleKey)

    lenDiff = canonicalLength - otherLength
    nlengthDiff = (1.0*lenDiff)/canonicalLength
    lenAfter = otherLength - start
    frameshift = 1

    #This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33
    if int(tokens[0].split("_")[-1].split("=")[1]) % 3 == 0:
        frameshift = 0

    lengthAExon = int(tokens[0].split("-")[-1].split("=")[1])

    featureLine = repr(frameshift) + ", " + repr(lengthAExon) + ", " + repr(lenDiff) + ", " + repr(nlengthDiff) + ", " + repr(lenAfter) + ", " + convList(coreFeatures) + ", " + convList(disorderFeatures) + ", " + convList(domainFeatures) + ", " + convList(elmFeatures) + ", " + convList(mutationFeatures) + ", " + convList(ptmFeatures) + ", " + convList(transmemFeatures) + ", " + convList(degreeFeatures) + ", " + convList(sequenceConFeatures) + ", " + convList(eventConFeatures)  + ", " + convList(tsFeatures)



    if asid[0] == "I": output_positiveI.append([dualKey, featureLine])

    if asid[0] == "E": output_positiveE.append([dualKey, featureLine])


header = "frameshift, spliceLength, lenDiff, nlenDiff, lenAfter, coreC1, coreA, coreC2, canCore, disorderC1, disorderA, disorderC2, canDisorder, otherDisorder, otherADis, domainC1, domainA, domainC2, canDomain, otherDomain, otherADom, enzymatic, elmC1, elmA, elmC2, canElm, mutationC1, mutationA, mutationC2, canMutation, ptmC1, ptmA, ptmC2, canPtm, transmemC1, transmemA, transmemC2, canTransmem, degree, seqConAve, seqConMin, seqConMax, classifier, spSpec, consAS, gspec, ts"


print >> writeToAll, "IE, " + header
for t in output_positiveI:
    print >> writeNamesAll, t[0]
    print >> writeToAll, "1, " + t[1]

for t in output_positiveE:
    print >> writeNamesAll, t[0]
    print >> writeToAll, "0, " + t[1]

