#generates event conservation feature table
import sys,os

#################
#### FUNCTIONS

def conv(value):
	if value:
		return "1"
	else:
		return "0"

#################
#####PATH VARIABLE

try:
	PulsePATH = os.environ['PULSE_PATH']
except KeyError:
	print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
	print 'TRYNG WITH WORKING DIR'
	PulsePATH = os.getcwd()

#################
##### INPUT FILES

#Load Events info
try:
	readFrom2 = open(PulsePATH+'/param_files/phastCons.hg18.bed', 'r')
	readFrom3 = open(PulsePATH+'/param_files/eventConservation.txt', 'r')

#Read the file with the location of the Alternative Splicing Events
	as_location_file = sys.argv[1]
	remaped_ccordinates = sys.argv[2]


	readFrom = open(as_location_file, 'r')
	readFrom1 = open(remaped_ccordinates, 'r')

except IndexError:
	print 'Alternative Splicing Location and the Remap hg18 file must be provided'
	sys.exit()

#################
#### OUTPUT FILES

writeTo = open('eventCon_read.out', 'w')

#### Init VARs

old_to_new = {}
new_to_nm = {}
nm_to_quad = {}

#################
#### MAIN

for line in readFrom3:
	tokens = line.split()

	key = tokens[0]
	quad = map(conv, ["Classifier" in tokens[1], "Sp-spec" in tokens[1], "Cons-AS" in tokens[1], "G-spec" in tokens[1]])

	nm_to_quad[key] = quad


for line in readFrom1:
	tokens = line.split()

	old_key = tokens[3] + "//" + tokens[7] + "//" + tokens[8]
	new_key = tokens[4] + "//" + repr(int(tokens[12])-1) + "//" + tokens[13]

	old_to_new[old_key] = new_key


for line in readFrom2:
	tokens = line.split()

	new_key = tokens[0] + "//" + tokens[1] + "//" + tokens[2]

	new_to_nm[new_key] = tokens[3]


for line in readFrom:
	tokens = line.split()

	chromosome = tokens[0]
	start = tokens[1]
	end = tokens[2]
	asid = tokens[3]
	type = tokens[4]
	strand = tokens[5]

	if type == "A":
		try:
			key = "chr" + chromosome + "//" + start + "//" + end
			new_key = old_to_new[key]
			nm = new_to_nm[new_key]
			quad = ["0","0","0","0"]

			if nm_to_quad.has_key(nm):
				quad = nm_to_quad[nm]
			print >> writeTo, asid + "\t" + quad[0] + "\t" + quad[1] + "\t" + quad[2] + "\t" + quad[3]
		except KeyError:
			print key + "\t" + new_key

