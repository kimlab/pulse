#generates network features using gene names
import sys,os

#################
#####PATH VARIABLE

try:
    PulsePATH = os.environ['PULSE_PATH']
except KeyError:
    print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
    print 'TRYNG WITH WORKING DIR'
    PulsePATH = os.getcwd()

#################
##### INPUT FILES
try:
    file_geneid_index = PulsePATH+'/param_files/uniprot_genewiki.index'
    readFrom1 = open(PulsePATH+'/param_files/degree.csv', 'r')

#Read the file with the maping between a splicing and its anchor
    map_file = sys.argv[1]

    file_anchor_map = map_file
except IndexError:
    print 'Map file must be provided'
    sys.exit()


#################
#### OUTPUT FILES

writeTo2 = open('degree_read.out', 'w')

#################
#### Init VARs

dict_uniprot = {}
name_to_unigene = {}
dict_genewiki = {}


#################
#### MAIN

#LOAD TABLE Gene Name - Uniprot_ID
#Return a Dict key:Uniprot Value: GeneWiki Name
for line in open(file_geneid_index,'r'):
    tab_data = line.split('\t')

    dict_genewiki[tab_data[1].strip()] = tab_data[0]


#LOAD TABLE ISOFORM-ANCHOR_Uniprot
#Return a Dict key:Uniprot Value:Alt Splt ID
for line in open(file_anchor_map,'r'):
    tab_data = line.split('\t')
    #Key:Uniprot-Anchor Value:Alt Splic Event ID
    #PARSING ID
    asid = tab_data[0]
    dict_uniprot[tab_data[1].strip()] = asid
    #PARSING ID
    #look if there is a uniprot->gene
    for geneid,uniprot in dict_genewiki.iteritems():
        if tab_data[1] == uniprot:

            name_to_unigene[geneid] = asid
            break


#LOAD degree File select matches and print score

for line in readFrom1:
	tokens = line.split(",")
	name = tokens[1].strip()
	score = tokens[2].strip()

	if name_to_unigene.has_key(name):
		asid = name_to_unigene[name]
		print >> writeTo2, asid + "\t" + score
