#reads uniprot core file and generates core features
import sys,os

#################
#### FUNCTIONS


def score_differences(mapping, uniprot, start, end):
	count = 0
	if mapping.has_key(uniprot):
		if start <= end:
			for i in range(start,end+1):
				if not mapping[uniprot].has_key(i):
					pass
				elif mapping[uniprot][i] == "*":
					count = count + 1
			return (1.0*count)/(end - start + 1)

	return count


#################
#####PATH VARIABLE

try:
	PulsePATH = os.environ['PULSE_PATH']
except KeyError:
	print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
	print 'TRYNG WITH WORKING DIR'
	PulsePATH = os.getcwd()

#################
##### INPUT FILES

try:
#Read the file with the maping between a splicing and its anchor
	map_file = sys.argv[1]
	sable_output = sys.argv[2]

	readFrom1 = open(map_file, 'r')
	readFrom = open(sable_output,'r')

except IndexError:
	print 'Map output and Sable output must be provided'
	sys.exit()

#################
#### OUTPUT FILES

writeTo = open('core_read.out', 'w')

#################
#### Init VARs


uniprot_to_index_to_core = {}
uniprot_to_index_to_core_s = {}


for line in readFrom:
	tokens = line.split()

	try:
		#PARSING ID
		prot = tokens[0]
		index = int(tokens[1])
		core = tokens[2]
		#PARSING ID
		if uniprot_to_index_to_core.has_key(prot):
			uniprot_to_index_to_core[prot][index] = core
		else:
			uniprot_to_index_to_core[prot] = {index:core}
	except ValueError:
		print "Cannot parse: " + line[0:len(line)-1]


for line in readFrom1:
	tokens = line.split()
	#PARSING ID
	#Customize the line below if is need it
	#it depends of how it is the label of th events
	#This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33
	asid = tokens[0].split("_")[0]
	#PARSING ID
	prot = tokens[1]
	sstart = int(tokens[2])
	start = int(tokens[3])
	end = int(tokens[4])
	eend = int(tokens[5])

	#if (end - start + 1) <= 0:
	#	continue

	roughALength = int(int(tokens[0].split("_")[-1].split("=")[1])/3)

	if asid[0] == "I":
		roughALength = 0

	c1Count = 0
	aCount = 0
	c2Count = 0
	canonicalAbsolute = 0


	if  uniprot_to_index_to_core.has_key(prot):

		c1Count = score_differences(uniprot_to_index_to_core, prot, sstart, start)
		aCount = score_differences(uniprot_to_index_to_core, prot, start, end)
		c2Count = score_differences(uniprot_to_index_to_core, prot, end, eend)

		#use teh new prot length provided
		protLen = int(line.split("\t")[7].strip())

		canonicalAbsolute = score_differences(uniprot_to_index_to_core, prot, 1, protLen)


	print >> writeTo, tokens[0] + "\t" + prot + "\t" + repr(c1Count) + "\t" + repr(aCount) + "\t" + repr(c2Count) + "\t" + repr(canonicalAbsolute)
