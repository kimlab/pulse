#reads uniprot domain file and generates domain features
import os,sys

#################
#### FUNCTIONS

def score_differences(mapping, uniprot, start, end):
	count = 0
	enzymatic = 0
	if mapping.has_key(uniprot):
		if start <= end:
			for i in range(start,end+1):
				if not mapping[uniprot].has_key(i):
					pass
				elif mapping[uniprot][i][0] == "*":
					if mapping[uniprot][i][1]:
						enzymatic = 1
					count = count + 1
			return [(1.0*count)/(end - start + 1),enzymatic]

	return [count, 0]


#################
#####PATH VARIABLE

try:
	PulsePATH = os.environ['PULSE_PATH']
except KeyError:
	print 'ENVIOROMENT VARIABLE PULSE_PATH not set up'
	print 'TRYNG WITH WORKING DIR'
	PulsePATH = os.getcwd()

#################
##### INPUT FILES
try:
	readFrom2 = open(PulsePATH+'/param_files/pfam_special.inp', 'r')

	map_file = sys.argv[1]
	pfamscan_output = sys.argv[2]

	readFrom1 = open(map_file, 'r')
	readFrom = open(pfamscan_output, 'r')

except IndexError:
	print 'Map and pfamscan output files must be provided'
	sys.exit()


#################
#### OUTPUT FILES

writeTo = open('domain_read.out', 'w')

#### Init VARs

uniprot_to_index_to_domain = {}
pfam_special = {}


###############
#### MAIN


for line in readFrom2:
	tokens = line.split()

	pfam = tokens[1]
	pfam_special[pfam] = True


for line in readFrom:
	tokens = line.split()

	try:
		uniprot = tokens[0]

		if "|" in uniprot:
			uniprot = uniprot.split("|")[1]

		start = int(tokens[1])
		end = int(tokens[2])

		pfam = tokens[5].split(".")[0]
		enzymatic = pfam_special.has_key(pfam)

		for i in range(start, end+1):

			if uniprot_to_index_to_domain.has_key(uniprot):
				uniprot_to_index_to_domain[uniprot][i] = ["*",enzymatic]
			else:
				uniprot_to_index_to_domain[uniprot] = {i:["*",enzymatic]}
	except ValueError:
		print "Cannot parse: " + line[0:len(line)-1]
	except IndexError:
		print "Index Error: " + line.strip()


for line in readFrom1:
	tokens = line.split()
	#PARSING ID
	#Customize the line below if is need it
	#it depends of how it is the label of th events
	#This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33

	asid = tokens[0].split("_")[0]
	prot = tokens[1]
	#PARSING ID
	sstart = int(tokens[2])
	start = int(tokens[3])
	end = int(tokens[4])
	eend = int(tokens[5])

	roughALength = int(int(tokens[0].split("_")[-1].split("=")[1])/3)

	if asid[0] == "I":
		roughALength = 0


	c1Count = 0
	aCount = 0
	c2Count = 0
	canonicalAbsolute = 0
	otherAbsolute = 0
	otherA = 0

	if not uniprot_to_index_to_domain.has_key(prot):
		c1Count = 0
		aCount = 0
		c2Count = 0
		canonicalAbsolute = 0
		otherAbsolute = 0
		otherA = 0
		enzymatic = 0
	else:
		c1Count = score_differences(uniprot_to_index_to_domain, prot, sstart, start)[0]
		aCount = score_differences(uniprot_to_index_to_domain, prot, start, end)[0]
		c2Count = score_differences(uniprot_to_index_to_domain, prot, end, eend)[0]

		enzymatic = score_differences(uniprot_to_index_to_domain, prot, start, end)[1]

		#use teh new prot length provided
		protLen = int(line.split("\t")[7].strip())
		otherProt = asid #or token[0] Customize this line if is need it for test [asid+'//'+prot+'-ST']
		otherProtLen = int(line.split("\t")[8].strip())
		canonicalAbsolute = score_differences(uniprot_to_index_to_domain, prot, 1, protLen)[0]
		otherAbsolute = score_differences(uniprot_to_index_to_domain, otherProt, 1, otherProtLen)[0]

		otherAEnd = start + roughALength
		if otherAEnd > otherProtLen:
			otherAEnd = otherProtLen


		otherA = score_differences(uniprot_to_index_to_domain, otherProt, start, otherAEnd)[0]



	print >> writeTo, tokens[0] + "\t" + prot + "\t" + repr(c1Count) + "\t" + repr(aCount) + "\t" + repr(c2Count) + "\t" + repr(canonicalAbsolute) + "\t" + repr(otherAbsolute) + "\t" + repr(otherA) + "\t" + repr(enzymatic)


