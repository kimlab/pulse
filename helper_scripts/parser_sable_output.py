import sys

#init Var
prot_seq = ''
prot_id = ''
prot_pvalue = ''
prot_asa = ''

#init Tags

Tag_reader = False
Tag_seq = False
Tag_asa = False
Tag_pvalue = False
Tag_num = False


out_sable = sys.argv[1]

def transform_asa(data):

	if int(data[2]) >= 7 and int(data[1]) <= 1:
		return "*"
	else:
		return "."


def print_asa(prot_id,data):

	index = 1

	for entry in data:
		asa = transform_asa(entry)
		print "%s\t%i\t%s" % (prot_id,index,asa)
		index +=1

	return

with open(out_sable) as input_file:
	for line in input_file:
		#print line[0:5]
		#print line[0:3]

		if line[0:5] == "Query":
			prot_id = line.split(':')[1].strip()

			Tag_reader = True


		elif line[0] == ">" and Tag_reader:
			Tag_seq = True


		elif Tag_seq:

			prot_seq += line.strip()
			Tag_asa = True
			Tag_seq = False

		elif Tag_asa:
			prot_asa += line.strip()
			Tag_asa = False
			Tag_pvalue = True

		elif Tag_pvalue:
			prot_pvalue += line.strip()

			Tag_pvalue = False
		
		elif line[0:3] == "END" and Tag_reader:
			Tag_reader = False
			Tag_seq = False

			#print prot_id, '  --'
			#print prot_seq
			total_data = zip(prot_seq,prot_asa,prot_pvalue)
			#print total_data
			print_asa(prot_id,total_data)
			prot_seq = ''
			prot_asa = ''
			prot_pvalue = ''
			prot_id = ''
