This is PULSE Pipeline Please:http://www.kimlab.org/ as well as our upcoming paper:
Title: Semi-supervised learning predicts approximately a third of the alternative splicing isoforms as novel functional proteins
Authors: Yanqi Hao, Recep Colak, Joan Teyra, Carles Corbi-Verge, Alexander Ignatchenko, Hannes Hahne, Mathias Wilhelm, Bernhard Kuster, Pascal Braun, Daisuke Kaida, Thomas Kislinger and Philip M. Kim

The pipeline have been written and tested with Python 2.7.6, R 3.1.2 and Linux ubuntu 13.04


REQUIREMENTS
---------------------------

Please, set up the $PULSE_PATH environment variable, with the complete path of Pulse pipeline (i.e export PULSE_PATH='/home/user/soft/pulse')

The following software need to be installed in order to generate the complete set of features

- pfam_scan.pl
    ftp://ftp.sanger.ac.uk/pub/databases/Pfam/Tools/OldPfamScan/HMMER2/pfam_scan.pl
- BLAST 2.2.26
    http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download
- Iupred
    http://iupred.enzim.hu/
- Sable
    http://sable.cchmc.org/


RUN PULSE PIPELINE
---------------------------

1)Preprocessing step:

In this step we anchor each splicing event you have mined to an Uniprot anchor.  
If you notice empty output files, please, check and pay special attention to the Id of the isoforms and edit the code if/when needed. 
The section of the code related with the Id parsing have been labeled with the following tag "PARSING ID".


    INPUT REQUIREMENTS:

        *BLAST OUPUT
            Blastx the Alternative splicing nucleotide sequence (only C1+E+C2.) against Uniprot.
            In order to get the right format for the script please execute blastX with the following command:

            $>blastall -p blastx -d ../folder/uniprot_sprot.fasta -i query_AS.out -o blastx_AS_events.out -v1 -b1 -m8

        *Amino acids Sequence of the putative isoforms in Fasta Format.
            Please in order to link events and seq, use the same id to identify both i.e isoforms_aminoacid.fasta


    RUN:

    filtermap.py to filter the blast query results and obtain the map file between the splicing events and the anchors.

        $>python filtermap.py blastx.output /path/to/uniprot.fasta /path/to/isoforms_aminoacid.fasta > output.log


    OUTPUT: <uniprot to splicing map>
    This is a map between the splicing event and its uniprot anchor

===========================================================================================================================

2)Generating features step:

Run your sequence scanners on self translated isoforms along with uniprot canonical isoforms (pfamscan, iupred, Sable). 
A sample of the Transmembrane, Linear motif, Post-transcriptional modifications, Mutations among other helper files for Human Uniprot sequence are provided (/pulse/param_files). 
In order to parse the features into a matrix, please run the following scripts:

    INPUT REQUIREMENTS:

            *Amino acids Sequence of the putative isoforms
                To extract features, Disorder and domains
            *Alternative Splicing location
                The HG19 coordinates of the Alternative splicing events with the following format
                Chromosome  Start       End         Event_ID        Type_exon       Strain
                10          27054147    27054247    "NBT2.3476.15"  C1                  -

            *<uniprot to splicing map>
                Event_ID            Anchor Uniprot ID   mapping         other_anchors   length_anchors      Length_Isoform
                I.3108_131=110=75   Q9P0P0              30  73  110 134 []              153                     81
                E.3107_87=260=68    Q86TG1              39  67  67  89  []              271                     70

            *<Remap file>
                A index remapping the Alternative splicing event  to hg18 coordinates. A helper script is provided, see below.
                Line:1  1   1   chr20   chr20   96  96  49558568    49558663    +   49558568    49558663    48991975    48992070    +   1.00000 First Pass  Primary Assembly
                Line:2  1   1   chr20   chr20   91  91  49557402    49557492    +   49557402    49557492    48990809    48990899    +   1.00000 First Pass  Primary Assembly


    RUN:

    $>python uniprotTransmem.py <uniprot to splicing map>
    $>python uniprotPTM.py <uniprot to splicing map>
    $>python uniprotElmRead.py <uniprot to splicing map>
    $>python uniprotDomainRead.py <uniprot to splicing map> <pfam_scan.pl output of the anchor and isoforms>
    $>python uniprotDisorder.py <uniprot to splicing map> <iupred output of the anchor and isoforms>
    $>python uniprotCore.py <uniprot to splicing map> <sable output of the anchor>
    $>python mutationFeatures.py <uniprot to splicing map>
    $>python seqConservation.py <Alternative Splicing Location file> <Remap file>
    $>python eventConservation.py <Alternative Splicing Location file> <Remap file>
    $>python generateNetworkFeatureFile.py <uniprot to splicing map>


    Finally run generate_ML_input.py to  to generate feature matrix for the machine algorithm.

    OUTPUT: Matrix of features <features file>
            Label of each isoform <names file>

    These scripts need to be ran after R finishes the machine learning part.  These are not actually part of the pipeline, they're
used for analysis of results which may differ depending on what you're doing.  See individual files for details.

3)Scoring step:

Run machine learning algorithm on the feature table for final results. Please edit the PATH of the inputfiles, working directory and model file if needed.

    $>Rscript pulse_score_algorithm.r


===========================================================================================================================

HELPER SCRIPTS

    - conservationConversionQuery.py is used to generate the query file to generate an hg19 to hg18 mapping online. http://www.ncbi.nlm.nih.gov/genome/tools/remap
    - generate_iupred_file.py is used to call iupred and score all the seq in a fasta file. the ouput is suitable for the features scripts
    - parser_sable_output is used to parse the Sable output in a format suitable for the uniprotcore.py script

===========================================================================================================================

TEST/Features

    In this folder you will find some example files to run the script.

    INPUT FILES:

        exon_maps.lite.txt <uniprot to splicing map>
        ASlocations.txt <Alternative Splicing location>
        remap_query.txt <remapping the Alternative splicing events  to hg18>
        sable_results.lite.out <Sable output>
        isoforms.fasta <Aminoacid seq of the isoforms>
        iupred_results.lite.out <iupred output>
        pfam_scan_results.out <pfam_scan.pl output>


    OUTPUTFILES:
    Precalculated output files to compare or test just the labeling step.

        core_read.old.lite.results.out
        degree_read.old.lite.results.out
        disorder_read.old.lite.results.out
        domain_read.old.lite.results.out
        elm_read.old.lite.results.out
        eventCon_read.old.lite.results.out
        mutation_read.old.lite.results.out
        ptm_read.old.lite.results.out
        sequenceCon_read.old.lite.results.out
        transmem_read.old.lite.results.out

TEST/Preprocess
    In this folder you will find some example files to run the script.

    INPUT FILES:
        blastx_lite.output.out
            Blastx output of the Alternative events
        pSeq_full_len_iso_Non-TNBC.fas
            Poll of the sequences of the isoforms

    OUTPUT:
        uniprot_exon_indices_map.results.lite.out

TEST/Scoring/ML_input_example

    featuresHeader.txt
        an example of a input for the classifier
